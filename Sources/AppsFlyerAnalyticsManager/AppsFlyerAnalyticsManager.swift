import AnalyticsManagers
import AppsFlyerLib
import CryptoSwift

public final class AppsFlyerAnalyticsManager {
    private let appsFlyerLib: AppsFlyerLib
    
    public init(appsFlyerLib: AppsFlyerLib, setup: (AppsFlyerLib) -> Void) {
        self.appsFlyerLib = appsFlyerLib
        setup(appsFlyerLib)
    }
}


// MARK: AnalyticsManager
extension AppsFlyerAnalyticsManager: AnalyticsManager {
    public func identify(user: String, domain: String?) {
        appsFlyerLib.customerUserID = user.sha256()
    }
    
    public func logEvent(eventName: String, params: [String: String]) {
        appsFlyerLib.logEvent(name: eventName, values: params)
    }
    
    public func getIdentityParams() -> [String: String] {
        [
            "af_id": appsFlyerLib.getAppsFlyerUID(),
            "af_cuid": appsFlyerLib.customerUserID
        ]
        .reduce(into: [:], { result, pair in
            if let value = pair.value {
                result[pair.key] = value
            }
        })
    }
    
    public func start() {
        appsFlyerLib.start { dictionary, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let dictionary = dictionary {
                print(dictionary)
                return
            }
            
            assertionFailure("AppsFlyerLib start unexpected behavior: both error and dictionary were nil")
        }
    }
}
