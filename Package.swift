// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AppsFlyerAnalyticsManager",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "AppsFlyerAnalyticsManager",
            targets: ["AppsFlyerAnalyticsManager"]),
    ],
    dependencies: [
        .package(
            url: "https://gitlab.com/aleksei.muraveinik/analyticsmanagers",
            branch: "master"
        ),
        .package(
            url: "https://github.com/AppsFlyerSDK/AppsFlyerFramework",
            exact: .init(6, 8, 1)
        ),
        .package(
            url: "https://github.com/krzyzanowskim/CryptoSwift",
            exact: .init(1, 6, 0)
        )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "AppsFlyerAnalyticsManager",
            dependencies: [
                .product(name: "AnalyticsManagers", package: "analyticsmanagers"),
                .product(name: "AppsFlyerLib", package: "AppsFlyerFramework"),
                .product(name: "CryptoSwift", package: "CryptoSwift")
            ]),
    ]
)
